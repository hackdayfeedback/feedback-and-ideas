/**
 * GET /
 * Form page.
 */
exports.showForm = function(req, res) {
  res.render('add', {
    title: 'Submit Feedback & Idea' 
  });
};
/**
 * Module dependencies.
 */
var express = require('express');
var compress = require('compression');
var session = require('express-session');
var bodyParser = require('body-parser');
var logger = require('morgan');
var errorHandler = require('errorhandler');
var lusca = require('lusca');
var methodOverride = require('method-override');
var dotenv = require('dotenv');
var MongoStore = require('connect-mongo/es5')(session);
var flash = require('express-flash');
var path = require('path');
var mongoose = require('mongoose');
var passport = require('passport');
var expressValidator = require('express-validator');
var sass = require('node-sass-middleware');
var multer = require('multer');
var upload = multer({ dest: path.join(__dirname, 'uploads') });
var assert = require('assert');
var MongoClient = require('mongodb').MongoClient;

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 *
 * Default path: .env (You can remove the path argument entirely, after renaming `.env.example` to `.env`)
 */
dotenv.load({ path: '.env.example' });

/**
 * Controllers (route handlers).
 */
var homeController = require('./controllers/home');
var userController = require('./controllers/user');
var apiController = require('./controllers/api');
var contactController = require('./controllers/contact');
var formController = require('./controllers/add');

/**
 * API keys and Passport configuration.
 */
var passportConfig = require('./config/passport');

/**
 * Create Express server.
 */
var app = express();

var suggestionArray = [];
/**
 * Connect to MongoDB.
 */
// mongoose.connect(process.env.MONGODB || process.env.MONGOLAB_URI);
// mongoose.connection.on('error', function() {
//   console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
//   process.exit(1);
// });

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(compress());
app.use(sass({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  sourceMap: true
}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(methodOverride());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  store: new MongoStore({
    url: process.env.MONGODB || process.env.MONGOLAB_URI,
    autoReconnect: true
  })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.use(function(req, res, next) {
  res.locals.user = req.user;
  next();
});
app.use(function(req, res, next) {
  // After successful login, redirect back to /api, /contact or /
  if (/(api)|(contact)|(^\/$)/i.test(req.path)) {
    req.session.returnTo = req.path;
  }
  next();
});
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));

/**
 * Primary app routes.
 */
app.get('/', homeController.index);
app.get('/add', formController.showForm);

/**
 * API examples routes.
 */


///////////////////////////////////////// ROUTES ///////////////////////////////////////////////////////////////////

app.get('/getSuggestions', function(req, res) {
    
    MongoClient.connect(process.env.MONGODB || process.env.MONGOLAB_URI, function(err, db) {
        assert.equal(null, err);
        for(var i = 0; i < suggestionArray.length; i++){
           suggestionArray.pop();
        }
        findSuggestions(db, function() {
            db.close();
            console.log(suggestionArray);
            res.send(suggestionArray);
        });
        
    });  

});

app.post('/putSuggestion', function (req, res) {
    var suggestion = req.body;
    suggestion.time = Date();
    suggestion.thumbsUp = {count: 0, emails: [] };
    suggestion.thumbsDown = {count: 0, emails: [] };
    suggestion.comments = [];
    
    MongoClient.connect(process.env.MONGODB || process.env.MONGOLAB_URI, function(err, db) {
        assert.equal(null, err);
        insertDocument(suggestion, db, function() {
            db.close();
        });
    });
  res.json(suggestion);
});

app.post('/addComment', function (req, res) {
   var comment = req.body;
   var title = comment.Title;
   comment.time = Date();
   MongoClient.connect(process.env.MONGODB || process.env.MONGOLAB_URI, function(err, db) {

        var collection = db.collection('suggestions');
        collection.updateOne(
            {Title: title},
            { $push: { comments: comment } }
        );
        res.send("did it");     
    });
});

app.post('/thumbsUp', function (req, res) {
   MongoClient.connect(process.env.MONGODB || process.env.MONGOLAB_URI, function(err, db) {

        var collection = db.collection('suggestions');
        var opinion = req.body;
        var title = opinion.Title;
        var email = opinion.Email;
        
        MongoClient.connect(process.env.MONGODB || process.env.MONGOLAB_URI, function(err, db) {
            assert.equal(null, err);
            for(var i = 0; i < suggestionArray.length; i++){
            suggestionArray.pop();
            }
            findSuggestions(db, function() {
                db.close();
                console.log(suggestionArray);
            });
        });  
        
        for(var i = 0; i < suggestionArray.length; i++){
            if(suggestionArray[i].thumbsUp.emails.indexOf(email) < 0){
                res.send({'message': 'you already expressed an opinion about this'});
            }
            else{
                collection.updateOne(
                    {Title: title},
                    { $inc: { thumbsUp:{count: 1} } }
                );
                
                collection.updateOne(
                    {Title: title},
                    { $push: { thumbsUp:{emails: email } } }
                );  
                
                res.send("success");
            }
        }
        
        

    });
});

app.post('/thumbsDown', function (req, res) {
   MongoClient.connect(process.env.MONGODB || process.env.MONGOLAB_URI, function(err, db) {

        var collection = db.collection('suggestions');
        var opinion = req.body;
        var title = opinion.Title;
        var email = opinion.Email;
        
        
         MongoClient.connect(process.env.MONGODB || process.env.MONGOLAB_URI, function(err, db) {
            assert.equal(null, err);
            for(var i = 0; i < suggestionArray.length; i++){
            suggestionArray.pop();
            }
            findSuggestions(db, function() {
                db.close();
                console.log(suggestionArray);
            });
        });  
        
        for(var i = 0; i < suggestionArray.length; i++){
            if(suggestionArray[i].thumbsDown.emails.indexOf(email) < 0){
                res.send({'message': 'you already expressed an opinion about this'});
            }
            else{
                collection.updateOne(
                    {Title: title},
                    { $inc: { thumbsDown:{count: 1} } }
                );
                
                collection.updateOne(
                    {Title: title},
                    { $push: { thumbsDown: { emails: email } } }
                );  
                
                res.send("success");
            }
        }
        

    });
});

///////////////////////////////////////// FUNCTIONS ///////////////////////////////////////////////////////////////////

var insertDocument = function(document, db, callback) {
   db.collection('suggestions').insertOne( document, function(err, result) {
    assert.equal(err, null);
    console.log("Inserted a document into the suggestions collection.");
    callback();
  });
};



var findSuggestions = function(db, callback) {
   var cursor =db.collection('suggestions').find( );
   cursor.each(function(err, doc) {
      assert.equal(err, null);
      if (doc != null) {
          
         var singleSuggestion = {'title': doc.Title, 'content': doc.Suggestion,
          'category': doc.Category, 'time': doc.time, 'comments': doc.comments, 'thumbsUp': doc.thumbsUp, 'thumbsDown': doc.thumbsDown};
          
         suggestionArray.push(singleSuggestion);
         console.log("in loop  " , suggestionArray);
      } else {
        callback();
      }
   });
};

/**
 * Error Handler.
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
app.listen(app.get('port'), function() {
  console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});

module.exports = app;
